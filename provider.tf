# Set up provider details
variable "do_token" {}

variable "ssh_user" {}

variable "domain" {}

variable "subdom" {}

variable "size" {}

variable "region" {}

variable "image_slug" {}

variable "keys" {}

variable "private_key_path" {}

variable "ssh_fingerprint" {}

variable "public_key" {}

provider "digitalocean" {
  token = "${var.do_token}"
}
