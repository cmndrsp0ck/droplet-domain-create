#### Droplet and domain deployment

This is a quick Terraform script that will allow you to set up a DigitalOcean Droplet along with a domain name for testing. It will also set up an SSH user with sudo access.

#### Prerequisites

* You'll need to install [Terraform](https://www.terraform.io/downloads.html) which will be used to handle Droplet provisioning.
* We're going to need a DigitalOcean API key. The steps to generate a DigitalOcean API key can be found [here](https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-api-v2#how-to-generate-a-personal-access-token).

#### Configuring

Let's get Terraform ready to deploy. We're going to be using **terraform.tfvars** to store values required such as API key, domain name, sub-domain, SSH data, Drople size, image id, etc. The sample file **terraform.tfvars.sample** has been supplied, just remember to remove the appended _.sample_. Once you have your all of the variables set, Terraform should be able to authenticate and deploy your Droplet and create a domain record.

#### Deployment

Now that you have all the variables set up, the last step is to simple create a deployment plan which is done by running `terraform plan` then execute that plan by running `terraform apply`.

That should take about one minute to complete and once it's done, you will have access to a new DigitalOcean Droplet with a domain name assigned to it and a DNS record pointing to its public IP address.
