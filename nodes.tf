#

resource "digitalocean_droplet" "node" {
  image              = "${var.image_slug}"
  name               = "${var.domain}"
  region             = "${var.region}"
  size               = "${var.size}"
  private_networking = true
  ipv6               = true
  ssh_keys           = ["${var.keys}"]
  user_data          = "${data.template_file.user_data.rendered}"

  connection {
    user     = "root"
    type     = "ssh"
    key_file = "${var.private_key_path}"
    timeout  = "2m"
  }
}

resource "digitalocean_domain" "default" {
  name       = "${var.domain}"
  ip_address = "${digitalocean_droplet.node.ipv4_address}"
}

resource "digitalocean_record" "subdomain" {
  domain = "${digitalocean_domain.default.name}"
  type   = "A"
  name   = "${var.subdom}"
  value  = "${digitalocean_droplet.node.ipv4_address}"
}

resource "digitalocean_record" "v6_address" {
  domain = "${var.domain}"
  type   = "AAAA"
  name   = "@"
  value  = "${digitalocean_droplet.node.ipv6_address}"
}

# You can use this section to set up a user
data "template_file" "user_data" {
  template = "${file("${path.module}/config/cloud-config.yaml")}"

  vars {
    public_key = "${var.public_key}"
    ssh_user   = "${var.ssh_user}"
  }
}
